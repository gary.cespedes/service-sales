package com.group.service.sales.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Gary A. Cespedes
 **/
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("sales-api")
                .select()
                .apis(RequestHandlerSelectors
                        .basePackage("com.sales.group.sales.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiEndPointsInfo())
                .useDefaultResponseMessages(false);
    }

    private ApiInfo apiEndPointsInfo() {

        return new ApiInfoBuilder()
                .title("Sales API")
                .description("Sales Management REST API")
                .contact(new Contact("Gary A. Cespedes","","garyCespedesH@gmail.com"))
                .version("0.0.2")
                .license("Apache 1.0").licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .build();
    }
}