package com.group.service.sales.demo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Gary A. Cespedes
 **/
@Api(value="Employee Management System",
        description="Operations pertaining to employee in Employee Management System")
@RestController
@RequestScope
public class EmployeeController {
    @ApiOperation(
            value = "Create an employee"
    )
    @ApiResponses({
            @ApiResponse(
                    code = 401,
                    message = "Unauthorized to create account"
            )
    })
    public void createEmployee() {

    }
}
