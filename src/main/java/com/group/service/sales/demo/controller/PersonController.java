package com.group.service.sales.demo.controller;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Gary A. Cespedes
 **/
@Api(
        tags = "Person",
        description = "Operations over persons"
)
@RestController
@RequestScope
public class PersonController {
}