package com.group.service.sales.demo.model.domain;

/**
 * @author Gary A. Cespedes
 **/
public enum Male {
    WOMAN,
    MAN
}
